<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z663RlYNZyP8mE5BiLkoojXaG9oxSNavd1RaGTAfehh+yy40YOcdF17c0zfuhoPC28xj2TnB7m9Dkl4pN00JhA==');
define('SECURE_AUTH_KEY',  'DMLF8PH/u9ZcvjR+1mH0GCuF+iq4cGFrwSEv/0kchInAX1QzTJDM4XOoCgsd0uYXn9yHGwKi1JJYJ2NWgqV+dA==');
define('LOGGED_IN_KEY',    '52JuZqjCMYWNc/MYC2Z7GIa3EGPd20FIlGdlfGrQb3d249XeO8MyqQQNuBK/L8tzrM49r0SMGefsvWKQiuqPWQ==');
define('NONCE_KEY',        'cfTTzOg8M5pHypaem+UEQVfHlbA37UQrC+aWs+V+kPujyWVG4TyQVwdeUbsTObTDJjBQo60jNFYHu3lflUG5cg==');
define('AUTH_SALT',        'xJiiKMVCkyIJn1Tn2GxkflRE7c9H9uorlIZZmvTIBH8s7iq6JPOzPwE7LlyXoK5Opv6H6Y9gw06hsvaGhp/ohg==');
define('SECURE_AUTH_SALT', 'S3I8/YCScMKtvxuBsmTMeMrSbF7IUPcDmETPDLCV8LxKD6s6H5QnB2ZN4xS5VqX7bPF4BmR7xs2jP3ODjbfyBg==');
define('LOGGED_IN_SALT',   'j4VcHtetcfeHAf1Orp5dOzjJttZNloYAeyWfgqROafCDQnOfbNHexUEWfL7EG4FVqTZdtXzKjUBuIZPIe8Dnbg==');
define('NONCE_SALT',       '0bzJISisxgFB0iVvT3EARgjpdDxhVawhDDaJg8PL629RD0ptxtLbUbi0WKG1SBnE/ts+4p2H1KE4u5Yto4C/Wg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
