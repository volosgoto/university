<?php

//add styles
  add_action("wp_enqueue_scripts", "university_files");
  
  //mictotime() for disablig cashing
  function university_files(){
    wp_enqueue_script('main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true  );
    wp_enqueue_style("custom-google-font", "//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i");
    wp_enqueue_style("font-awesome", "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    wp_enqueue_style("univesity_maim_styles", get_stylesheet_uri(), null, microtime());
    
  }

  add_action( 'after_setup_theme', 'university_features' );
  function university_features(){
    register_nav_menu('headerMenuLocation', 'Header menu location');
    register_nav_menu('footerLocation_01', 'Footer location one');
    register_nav_menu('footerLocation_02', 'Footer location two');
    add_theme_support('title-tag');
  }
?>
